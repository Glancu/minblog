<?php

namespace BlogBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

class User extends BaseUser
{
    protected $id;

    private $sex;

    private $age;

    private $township;

    private $achievements;

    private $points;

    private $articles;

    public function setsex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    public function getsex()
    {
        return $this->sex;
    }

    public function setage($age)
    {
        $this->age = $age;

        return $this;
    }

    public function getage()
    {
        return $this->age;
    }

    public function settownship($township)
    {
        $this->township = $township;

        return $this;
    }

    public function gettownship()
    {
        return $this->township;
    }

    public function getpoints()
    {
        return $this->points;
    }

    public function setpoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->achievements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add achievement
     *
     * @param \BlogBundle\Entity\Achievements $achievement
     *
     * @return User
     */
    public function addAchievement(\BlogBundle\Entity\Achievements $achievement)
    {
        $this->achievements[] = $achievement;

        return $this;
    }

    /**
     * Remove achievement
     *
     * @param \BlogBundle\Entity\Achievements $achievement
     */
    public function removeAchievement(\BlogBundle\Entity\Achievements $achievement)
    {
        $this->achievements->removeElement($achievement);
    }

    /**
     * Get achievements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAchievements()
    {
        return $this->achievements;
    }

    /**
     * Add article
     *
     * @param \BlogBundle\Entity\Articles $article
     *
     * @return User
     */
    public function addArticle(\BlogBundle\Entity\Articles $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \BlogBundle\Entity\Articles $article
     */
    public function removeArticle(\BlogBundle\Entity\Articles $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }
}
