<?php

namespace BlogBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

class User extends BaseUser
{
    protected $id;

    private $sex;

    private $age;

    private $township;

    private $achievements;

    private $points;

    public function setsex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    public function getsex()
    {
        return $this->sex;
    }

    public function setage($age)
    {
        $this->age = $age;

        return $this;
    }

    public function getage()
    {
        return $this->age;
    }

    public function settownship($township)
    {
        $this->township = $township;

        return $this;
    }

    public function gettownship()
    {
        return $this->township;
    }

    public function getpoints()
    {
        return $this->points;
    }

    public function setpoints($points)
    {
        $this->points = $points;

        return $this;
    }

    public function __construct() {
        $this->achievements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addachievements(\BlogBundle\Entity\Achievements $achievements)
    {
        $this->achievements = $achievements;
    }
}