<?php
namespace BlogBundle\Controller;


use BlogBundle\Entity\Articles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Forms\ArticleForm;
use BlogBundle\Repository\ArticlesRepository;

class ArticleController extends Controller
{
    public function createAction(Request $request){
        $create = new Articles();
        $form = $this->createForm(ArticleForm::class, $create);

        if($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $title = $form['title']->getData();
            $description = $form['description']->getData();

            if (empty($title) || empty($description)) {
                $this->addFlash('error', 'Uzupełnij wszystkie pola.');
                return $this->redirectToRoute('blog_article_add');
            }

            $date = new\DateTime('now');
            $user = $this->getUser()->getId();

            $create->settitle($title);
            $create->setDescription($description);
            $create->setCreated($date);
            $create->setAuthor($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($create);
            $em->flush();

            $this->addFlash('notice', 'Artykuł został dodany!');

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('BlogBundle:Articles:create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('BlogBundle:Articles')->getArticleResult();


        return $this->render('BlogBundle:Articles:list.html.twig', array(
            'articles' => $articles,
        ));
    }

    public function articleAction($id) {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('BlogBundle:Articles')->find($id);
        if(!$article){
            throw $this->createNotFoundException('Nie znaleziono artykułu.');
        }

        return $this->render('BlogBundle:Articles:article.html.twig', array(
            'article' => $article,
        ));
    }

    public function editAction(Request $request, $id) {
        $edit = $this->getDoctrine()->getRepository('BlogBundle:Articles')->find($id);

        $edit->setTitle($edit->getTitle());
        $edit->setdescription($edit->getdescription());

        $form = $this->createForm(ArticleForm::class, $edit);

        if($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $title = $form['title']->getData();
            $description = $form['description']->getData();

            $edit->settitle($title);
            $edit->setDescription($description);

            $em = $this->getDoctrine()->getManager();
            $em->persist($edit);
            $em->flush();

            $this->addFlash('notice', 'Artykuł został zaktualizowany!');

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('BlogBundle:Articles:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $delete = $em->getRepository('BlogBundle:Articles')->find($id);
        $em->remove($delete);
        $em->flush();

        $this->addFlash('notice', 'Artykuł został usunięty!');

        return $this->redirectToRoute('blog_article_list');
    }
}
