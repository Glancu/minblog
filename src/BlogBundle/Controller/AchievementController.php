<?php
namespace BlogBundle\Controller;


use BlogBundle\Entity\Achievements;
use BlogBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BlogBundle\Forms\AchievementForm;

class AchievementController extends Controller
{
    public function createAction(Request $request) {
        $create = new Achievements();

        $form = $this->createForm(AchievementForm::class, $create);

        if($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $title = $form['title']->getData();
            $description = $form['description']->getData();

            if (empty($title) || empty($description)) {
                $this->addFlash('error', 'Uzupełnij wszystkie pola.');
                return $this->redirectToRoute('blog_article_add');
            }

            $create->settitle($title);
            $create->setDescription($description);

            $em = $this->getDoctrine()->getManager();
            $em->persist($create);
            $em->flush();

            $this->addFlash('notice', 'Odznaka została dodana!');

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('BlogBundle:Achievements:create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id) {
        $edit = $this->getDoctrine()->getRepository('BlogBundle:Achievements')->find($id);

        $edit->setTitle($edit->getTitle());
        $edit->setdescription($edit->getdescription());

        $form = $this->createForm(AchievementForm::class, $edit);

        if($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $title = $form['title']->getData();
            $description = $form['description']->getData();

            $edit->settitle($title);
            $edit->setDescription($description);

            $em = $this->getDoctrine()->getManager();
            $em->persist($edit);
            $em->flush();

            $this->addFlash('notice', 'Odznaka została zaktualizowana!');

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('BlogBundle:Achievements:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $achievements = $em->createQueryBuilder()
            ->select('a')
            ->from('BlogBundle:Achievements', 'a')
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('BlogBundle:Achievements:list.html.twig', array(
            'achievements' => $achievements
        ));
    }

    public function achievementAction($id) {
        $em = $this->getDoctrine()->getManager();

        $achievement = $em->getRepository('BlogBundle:Achievements')->find($id);
        if(!$achievement){
            throw $this->createNotFoundException('Nie znaleziono odznaki.');
        }

        return $this->render('BlogBundle:Achievements:achievement.html.twig', array(
            'achievement' => $achievement
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $delete = $em->getRepository('BlogBundle:Achievements')->find($id);
        $em->remove($delete);
        $em->flush();

        $this->addFlash('notice', 'Odznaka została usunięta!');

        return $this->redirectToRoute('blog_achievement_list');
    }

    public function getAction($id) {
        $em = $this->getDoctrine()->getManager();

        $achievement = $em->getRepository('BlogBundle:Achievements')->find($id);
        $user = $this->getUser();
        $points = $achievement->getpoints();

        if($user->getPoints() >= $points) {
            $user = $this->getUser();

            $product = $achievement;
            $user->addAchievement($product);

            $user = $user-$points;

            $take_points = $em->getRepository('BlogBundle:User')->find($user->getId());
            $take_points->setPoints($user);

            $em->persist($user);
            $em->persist($product);
            $em->persist($take_points);
            $em->flush();

            $this->addFlash('notice', 'Odznaka została dodana');
            return $this->redirectToRoute('blog_achievement_list');
        } else {
            $this->addFlash('error', 'Nie posiadasz wystarczającej ilości punktów do otrzymania rangi');
            return $this->redirectToRoute('blog_achievement_list');
        }
    }
}