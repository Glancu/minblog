<?php
namespace BlogBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->createQueryBuilder()
            ->select('a')
            ->from('BlogBundle:Articles', 'a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('BlogBundle:Index:index.html.twig', array(
            'articles' => $articles
        ));
    }
}
