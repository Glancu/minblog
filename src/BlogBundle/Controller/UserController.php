<?php
namespace BlogBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function profileAction() {
        $em = $this->getDoctrine()->getManager();
        $achievements = $em->createQueryBuilder('u')
            ->select('u.id, a.id as id, a.title as title')
            ->from('BlogBundle:User', 'u')
            ->join('u.achievements', 'a')
            ->where('u.id ='.$this->getUser()->getId())
            ->getQuery()
            ->getResult();

        return $this->render('BlogBundle:User:profile.html.twig', array(
            'user' => $this->getUser(),
            'achievements' => $achievements
        ));
    }
}